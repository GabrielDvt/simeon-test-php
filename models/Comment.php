<?php

    class Comment {
        private $id;
        private $description;
        private $user_id;
        private $post_id;
        private $created_at;

        public function setDescription($description)
        {
            $this->description = $description;
        }

        public function setUser($user_id)
        {
            $this->user_id = $user_id;
        }

        public function setPost($post_id)
        {
            $this->post_id = $post_id;
        }

        public function getDescription()
        {
            return $this->description;
        }

        public function getUser()
        {
            return $this->user_id;
        }

        public function getPost()
        {
            return $this->post_id;
        }

    }


?>