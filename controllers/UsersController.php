<?php

include_once(__DIR__ . "/../db/dao/UserDAO.php");
include_once(__DIR__ . '/../models/User.php');

    class UsersController {

        public static function authenticateUser($credentials)
        {   
            if (!is_array($credentials)) {
                return false;
            }

            if (isset($credentials['email']) && isset($credentials['password'])) {
                $userDAO = new UserDAO();
                $users = $userDAO->getAll();
              
                $authenticated = false;

                if ($users && is_array($users) && count($users) > 0) {
                    foreach($users as $user) {
                        
                        if (isset($user['password']) && isset($user['email']) && $user['email'] === $credentials['email'] && $user['password'] === $credentials['password']) {
                            $authenticated = true;
                        } 
                    }
                }
            } else {
                $authenticated = false;
            }

            return $authenticated;
        }

        public static function userExists($email)
        {

            if (!$email) {
                return;
            }

            $user = new User();
            $user->setEmail($email);

            $userDAO = new UserDAO();

            $exists = $userDAO->userExists($user);

            return $exists;
        } 

        public static function register($name, $email, $password)
        {
            $user = new User();
            $user->setEmail($email);
            $user->setPassword($password);
            $user->setName($name);

            $userDAO = new UserDAO();
            $result = $userDAO->insert($user);

            return $result;
        }
    }

?>