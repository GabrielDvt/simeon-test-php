<?php
class Connection {
    private static $servername = "localhost";
    private static $username = "root";
    private static $password = "";
    private static $dbName = "simeon_test_php";    

    public static function connect()
    {
        $conn = new mysqli(self::$servername, self::$username, self::$password, self::$dbName);
    
        if ($conn->connect_error) {
          die("Falha ao conectar " . $conn->connect_error);
        }
        
        echo "conectado";
        return $conn;   
    }
    
    public static function disconnect($conn)
    {
        if ($conn) {
            $conn->close();
            
            echo "Desconectado.";
            return $conn;
        }

        echo "Conexão não enviada.";
        return null;
    }
}

?>