<?php

include("../Connection.php");
include("../../models/Post.php");

class PostDAO {
    public function __construct()
    {
        $this->conn = Connection::connect(); 
    }

    public function insert(Post $post)
    {
        try {

            echo json_encode($this->conn);
            $smtm = $this->conn->prepare("INSERT INTO posts(title, description, user_id) VALUES (?, ?, ?)");
            echo json_encode($smtm);
            
            $title = $post->getTitle();
            $description = $post->getDescription();
            $user = $post->getUser();

            $smtm->bind_param("ssi", $title, $description, $user);
            
            $smtm->execute();

        } catch (Exception $e) {
            echo ($e->getMessage());
        }
    }

    public function getAll()
    {
        try {

            echo json_encode($this->conn);
            $result = $this->conn->query("SELECT * FROM posts");
            
            if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                   echo "Name: " . $row["name"]. "<br>";
                }
            }

        } catch (Exception $e) {
            echo ($e->getMessage());
        }
    }

}

$postDao = new PostDAO();

echo json_encode($postDao->getAll());
// $post = new Post();
// $post->setDescription("teste");
// $post->setTitle("teste");
// $post->setUser(4);
// $postDao->insert($post);

?>