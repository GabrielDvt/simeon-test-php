<?php

include("../Connection.php");
include("../../models/Comment.php");

class CommentDAO {
    public function __construct()
    {
        $this->conn = Connection::connect(); 
    }

    public function insert(Comment $comment)
    {
        try {

            echo json_encode($this->conn);
            $smtm = $this->conn->prepare("INSERT INTO comments(description, user_id, post_id) VALUES (?, ?, ?)");
            echo json_encode($smtm);
            
            $description = $comment->getDescription();
            $user_id = $comment->getUser();
            $post_id = $comment->getPost();

            $smtm->bind_param("sii", $description, $user_id, $post_id);
            
            $smtm->execute();

        } catch (Exception $e) {
            echo ($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            echo json_encode($this->conn);
            $result = $this->conn->query("SELECT * FROM comments");
            
            if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                   echo "Name: " . $row["name"]. "<br>";
                }
            }

        } catch (Exception $e) {
            echo ($e->getMessage());
        }
    }

}


?>