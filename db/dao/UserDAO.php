<?php

include_once(__DIR__ . "/../Connection.php");
include_once(__DIR__ . "/../../models/User.php");

class UserDAO {
    public function __construct()
    {
        $this->conn = Connection::connect(); 
    }

    public function insert(User $user)
    {
        try {
            $smtm = $this->conn->prepare("INSERT INTO users(name, email, password) VALUES (?, ?, ?)");
            
            $name = $user->getName();
            $email = $user->getEmail();
            $password = $user->getPassword();

            $smtm->bind_param("sss", $name, $email, $password);
            
            $smtm->execute();

            return true;
        } catch (Exception $e) {
            echo ($e->getMessage());
            return false;
        }
    }

    public function getAll()
    {
        try {
            
            $result = $this->conn->query("SELECT * FROM users");
            
            $return = [];
            if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                   array_push($return, $row);
                    // echo "Name: " . $row["name"]. "<br>";
                }
            }

            return $return;
        } catch (Exception $e) {
            echo ($e->getMessage());
        }
    }

    public function userExists(User $user)
    {
        try {
            
            $smtm = $this->conn->prepare("SELECT * FROM users WHERE email = ?");
           
            $email = $user->getEmail();
           
            $smtm->bind_param("s", $email);
            
            $smtm->execute();

            $result = $smtm->get_result();
            
            $user = $result->fetch_assoc();

            if ($user) {
                return true;
            } else {
                return false;
            }

        } catch (Exception $e) {
            echo ($e->getMessage());
            return false;
        }
    }
}

?>