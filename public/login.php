
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Seja bem-vindo</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" />
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
    <div class="master-container">
        <div class="container-login-title">
            <h1 class="login-title">Bem-vindo ao Post Chat</h1>
        </div>
        
        <div class="container-login">
            <div class="container-login-header">
                <h3>Login</h3>
            </div>

            <div class="container-login-body">
                <form action="actions/authenticate.php" method="POST">
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" name="email" value="" class="form-control" id="email" aria-describedby="" placeholder="E-mail">
                    </div>

                    <div class="form-group form-group-password">
                        <label for="password">Senha</label>
                        <input type="password" name="password" value="" class="form-control" id="password" placeholder="Senha">
                    </div>

                    <div class="forgot-password">
                        <a class="signup" href="#" onClick="javascript:alert('Esta funcionalidade não estava no escopo.')"><small>Recuperar senha</small></a>
                    </div>
                    
                    <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                </form>

                <a class="signup" href="register.php">Cadastrar</a>
            </div>
        </div>
        
    </div>
</body>

</html>