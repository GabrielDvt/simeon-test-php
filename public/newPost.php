<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Seja bem-vindo</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" />
    
    <script src="https://cdn.tiny.cloud/1/54l6kg8464cky53toh3g7icuo4j1wo1fopn60ab8jq4ianfn/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
   
<div class="master-container-home">
    <nav class="navbar navbar-light bg-dark">
        <a class="navbar-brand" href="#">POST Chat</a>
        <form method="POST" action="actions/logout.php">
            <button type="submit" class="btn btn-link btn-logout">Logout</button>
        </form>
    </nav>

    <div class="new-post-container">
        <div class="posts-container">
            <form action="actions/submitPost.php" method="POST">
                <div class="form-group">
                    <label for="title">Título</label>
                    <input type="text" name="title" value="" class="form-control" id="title" aria-describedby="" placeholder="Título">
                </div>

                <div class="form-group form-group-password">
                    <label for="description">Descrição</label>
                    <textarea name="description" id="mytextarea"></textarea>
                </div>
                
                <button type="submit" class="btn btn-primary btn-block btn-post">Postar!</button>
            </form>
        </div>
    </div>

</div>
</body>

</html>