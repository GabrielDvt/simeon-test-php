<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Seja bem-vindo</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" />
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
    <div class="master-container">
        <div class="container-signup">
            <div class="container-signup-header">
                <h3>Cadastre-se</h3>
            </div>
    
            <div class="container-signup-body">
                <form action="actions/signUp.php" method="POST">
                
                    <div class="form-group">
                        <label for="name">Nome *</label>
                        <input type="text" name="name" value="" class="form-control" id="name" aria-describedby="" placeholder="Nome">
                    </div>
    
                    <div class="form-group">
                        <label for="email">E-mail *</label>
                        <input type="email" name="email" value="" class="form-control" id="email" aria-describedby="" placeholder="E-mail">
                    </div>
    
                    <div class="form-group">
                        <label for="password">Senha *</label>
                        <input type="password" name="password" value="" class="form-control" id="password" placeholder="Senha">
                    </div>
    
                    <div class="form-group">
                        <label for="password">Confirmar senha *</label>
                        <input type="password" name="confirmPassword" value="" class="form-control" id="password" placeholder="Confirmar senha">
                    </div>
    
                    <button type="submit" class="btn btn-primary btn-block">Cadastrar</button>
    
                    <div class="go-back">
                        <a class="signup" href="login.php"><small>Voltar</small></a>
                    </div>
                </form>
            </div>
        </div>
            <!-- <div class="errors">
                <ul>
                @foreach ($errors->all() as $error)
                    <li><small>{{ $error }}</small></li>
                @endforeach
                </ul>
            </div> -->
    </div>
</body>

</html>