<?php

    include_once(__DIR__ . "/../controllers/UsersController.php");
    
    $email = $_POST['email'];
    $password = $_POST['password'];
    $errors = false;
    
    if (!isset($email) || empty($email)) {
        $_SESSION['errors']['email'] = "O E-mail é obrigatório.";
        $errors = true;
    }

    if (!isset($password) || empty($password)) {
        $_SESSION['errors']['password'] = "A senha é obrigatória.";
        $errors = true;
    }

    if($errors) {
        header("Location: http://localhost/simeon-test-php/public/login.php");
    } else {
        $user = new User();
        

        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        $authenticated = UsersController::authenticateUser($credentials);

        if ($authenticated) {
            header("Location: http://localhost/simeon-test-php/public/index.php");
        } else {
            echo "usuário não existe";
            header("Location: http://localhost/simeon-test-php/public/login.php");
        }

        // if($user_exists) {
        //    
        // } else {
       
        // }

    }

?>