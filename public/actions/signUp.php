<?php

    include_once(__DIR__ . "/../../controllers/UsersController.php");

    if (!isset($_POST['name']) || !isset($_POST['email']) || !isset($_POST['password']) || !isset($_POST['confirmPassword'])) {
        header("Location: http://localhost/simeon-test-php/public/register.php");
    }
    
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $confirmPassword = $_POST['confirmPassword'];

    
    if (empty($name) || empty($email) || empty($password) || empty($confirmPassword) || $password !== $confirmPassword) {
        header("Location: http://localhost/simeon-test-php/public/register.php");
    }

    

    if (UsersController::userExists($email)) {
        header("Location: http://localhost/simeon-test-php/public/register.php");
    }

    $result = UsersController::register($name, $email, $password);

    if ($result) {
        header("Location: http://localhost/simeon-test-php/public/index.php");
    } else {
        header("Location: http://localhost/simeon-test-php/public/register.php");
    }

?>